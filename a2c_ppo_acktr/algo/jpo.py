import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np


class JPO():
    def __init__(self,
                 actor_critic,
                 clip_param,
                 ppo_epoch,
                 num_mini_batch,
                 value_loss_coef,
                 entropy_coef,
                 lr=None,
                 eps=None,
                 max_grad_norm=None,
                 use_clipped_value_loss=True,
                 conditioning_coef = 0.1,
                 jc_border_max=20, #default params
                 jc_border_min=1,
                 c_log_dir=None):
        
        """JPO must have:
           1)Some additiona Noise to state for compute jacobian
           2)Get output of model without activation"""
        
        
        self.actor_critic = actor_critic

        self.clip_param = clip_param
        self.ppo_epoch = ppo_epoch
        self.num_mini_batch = num_mini_batch

        self.value_loss_coef = value_loss_coef
        self.entropy_coef = entropy_coef
        self.conditioning_coef = conditioning_coef

        self.max_grad_norm = max_grad_norm
        self.use_clipped_value_loss = use_clipped_value_loss
        
        self.a_max = torch.tensor(jc_border_max).float().cuda()
        self.a_min = torch.tensor(jc_border_min).float().cuda()

        self.optimizer = optim.Adam(actor_critic.parameters(), lr=lr, eps=eps)
        self.c_log_dir = c_log_dir

    
    def noise_obs_batch(self, obs_batch):
        obs_batch = obs_batch.cpu().detach().numpy()
        step = np.random.normal(size=obs_batch.shape)
        step = step / np.linalg.norm(step)
        z_obs_batch = obs_batch + step
        z_obs_batch = torch.from_numpy(z_obs_batch).float().cuda()# use .to(self.device) soon 
        return z_obs_batch
    
    
    def jacobian_objective(self, obs_batch, z_obs_batch, policy_features, z_policy_features):
        q = torch.norm(policy_features - z_policy_features) / torch.norm(obs_batch - z_obs_batch)
        L_min = torch.pow(torch.min(q, self.a_min) - self.a_min, 2)
        L_max = torch.pow(torch.max(q, self.a_max) - self.a_max, 2)
        L = L_min + L_max
        return L, L_min, L_max, q
        
        
    def update(self, rollouts):
        advantages = rollouts.returns[:-1] - rollouts.value_preds[:-1]
        advantages = (advantages - advantages.mean()) / (
            advantages.std() + 1e-5)

        value_loss_epoch = 0
        action_loss_epoch = 0
        dist_entropy_epoch = 0

        for e in range(self.ppo_epoch):
            if self.actor_critic.is_recurrent:
                data_generator = rollouts.recurrent_generator(
                    advantages, self.num_mini_batch)
            else:
                data_generator = rollouts.feed_forward_generator(
                    advantages, self.num_mini_batch)

            for sample in data_generator:
                #create here observation with noise
                obs_batch, recurrent_hidden_states_batch, actions_batch, \
                   value_preds_batch, return_batch, masks_batch, old_action_log_probs_batch, \
                        adv_targ, old_q_batch = sample

                
                
                # Reshape to do in a single forward pass for all steps
                values, action_log_probs, dist_entropy, _, policy_features = self.actor_critic.evaluate_actions(
                    obs_batch, recurrent_hidden_states_batch,
                    masks_batch, actions_batch)
                
                
                # Make here one more evaluation but with noised input 
                #print('obs_batch',obs_batch)
                #z_obs_batch = self.noise_obs_batch(obs_batch)
                #print('z_obs_batch',z_obs_batch)
                #_, _, _, _, z_policy_features = self.actor_critic.evaluate_actions(
                #    z_obs_batch, recurrent_hidden_states_batch,
                #    masks_batch, actions_batch)

                #compute for new policy
                #L, L_min, L_max, q = self.jacobian_objective(obs_batch, z_obs_batch, policy_features, z_policy_features)
                #print(L)
                #L_min = L_min.cpu().detach().numpy()
                #L_max = L_max.cpu().detach().numpy()
                #L = L.cpu().detach().numpy()
                
                #betta = 1.0
                #old_q_batch = old_q_batch.cpu().detach().numpy()
                #q_value = np.mean(old_q_batch)
                #q_value = torch.mean(old_q_batch)
                
                # Work here with mini batches of Q
                # We mean Q inside a mini batch, 
                # may be minibatch size become too big, and mean over L become close to zero??
                #print('Q', old_q_batch)
                q_value = old_q_batch
                L_min = torch.pow(torch.min(q_value, self.a_min) - self.a_min, 2)
                L_max = torch.pow(torch.max(q_value, self.a_max) - self.a_max, 2)
                #print('L max', L_max)
                #print('L min', L_min)
                L = L_min + L_max
                L = torch.mean(L)
                L = L.cpu().detach().numpy()
                print('L', L)

                #print('betta', betta)
                
                #clip_param = self.clip_param  *  L / float(num_updates))
                
                #conditioning_clip = self.clip_param + self.conditioning_coef * L
                conditioning_clip = self.clip_param + L
                
                ratio = torch.exp(action_log_probs - old_action_log_probs_batch)
                surr1 = ratio * adv_targ
                surr2 = torch.clamp(ratio, 1.0 - conditioning_clip,
                                           1.0 + conditioning_clip) * adv_targ
                action_loss = -torch.min(surr1, surr2).mean()

                if self.use_clipped_value_loss:
                    value_pred_clipped = value_preds_batch + \
                        (values - value_preds_batch).clamp(-conditioning_clip,   conditioning_clip)
                    value_losses = (values - return_batch).pow(2)
                    value_losses_clipped = (value_pred_clipped - return_batch).pow(2)
                    value_loss = 0.5 * torch.max(value_losses, value_losses_clipped).mean()
                else:
                    value_loss = 0.5 * (return_batch - values).pow(2).mean()

                self.optimizer.zero_grad()
                (value_loss * self.value_loss_coef + action_loss - dist_entropy * self.entropy_coef).backward()
                nn.utils.clip_grad_norm_(self.actor_critic.parameters(),
                                         self.max_grad_norm)
                self.optimizer.step()

                value_loss_epoch += value_loss.item()
                action_loss_epoch += action_loss.item()
                dist_entropy_epoch += dist_entropy.item()
            f = open(self.c_log_dir+"c_log.txt", "a")
            f.write(str(L) + "\n")
            f.close()

                    
        num_updates = self.ppo_epoch * self.num_mini_batch

        value_loss_epoch /= num_updates
        action_loss_epoch /= num_updates
        dist_entropy_epoch /= num_updates

        return value_loss_epoch, action_loss_epoch, dist_entropy_epoch
